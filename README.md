ID Recognition
==============

Recognise data from an ID card in order to fill APD RIP fields. The model is then used in [protoapp-id-recognition](https://gitlab.com/ripadp/protoapp-id-recognition).

The pipeline we are creating will look like this:

- get a picture of an ID card (front or back)
- binary-segment the image to identify which pixels correspond to the card, using [AdivarekarBhumit/ID-Card-Segmentation](https://github.com/AdivarekarBhumit/ID-Card-Segmentation)
- extract text areas in the picture (using OpenCV)
  - get the convex hull of the segmented area
  - use polygon approximation on the convex hull to recreate the polygon of the card
  - correct the perspective of that polygon to get a rectangle with a standard aspect ratio; at this point we have a good picture of the card
  - apply any filters to remove overlays such as "RF" and the like
  - carve out the areas of the card containing the information we want (which will be the same for all cards)
- OCR the extracted text areas (using Tesseract)

This repository holds the tools to create and manage the segmentation dataset, to train the segmentation model, possibly to create the image filter, and to export the trained segmentation model to tf-lite.


## Setup

Using Anaconda to set up the environment (and [fish](https://fishshell.com/)):
- `conda env create -f environment-<PU>.lock.yml -n (basename (pwd))`
- `conda activate (basename (pwd))`

where `<PU>` is the type of processing unit you're using, either `cpu` or `gpu`.

To upgrade the environment and its definition file:
- `conda env create -f environment-<PU>.yml -n (basename (pwd))`
- `conda activate (basename (pwd))`
- `conda env export > environment-<PU>.lock.yml`


## Usage

`dataset.py`
- `splitvideo`: split a video into its frames, for manual annotation and addition to the dataset
- `annotate`: identify the ground truth segmentation for a given image
- `annotate_ocr`: identify polygons and ground truth of fields in corrected views of ID cards. TODO
- `pack`: pack the dataset for storage on a private location online. TODO
- `get`: download and install an annotated dataset (built with `pack`) from a url. TODO
- `preprocess`: preprocess all images and masks into npz files

`train.py`
- `segmentation`: train the segmentation model
- `filter`: build the filter for image cleaning. TODO

`recognise.py`: take an image and extract its fields, save its segmentation, the drawn inferred polygon, the corrected perpsective, the identified fields and their text
- `--fromtruth`: provide the ground truth segmentation to skip segmentation and polygon-detection
- `--debug`: save all intermediary images produced in the process

`export.py`: export the segmentation model to the tensorflow-lite format


## Next steps

- [ ] annotate lots of images with `dataset.py annotate` -- see the [wiki](https://gitlab.com/ripadp/id-recognition/wikis/home) for more on this
- [ ] implement `dataset.py annotate_ocr imagepath side fieldname`, storing the position of a field by %, along with the true value
- [ ] annotate a bunch of images with `dataset.py annotate_ocr`, then take the bounding box of boxes
- [ ] implement tesseract pipeline to finish the `recognise.py` script (also test what the average image looks like for filtering)
- [ ] add test score for tesseract fields to `train.py`
