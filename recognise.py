import os
import json

import click
import cv2
import numpy as np

from recognition import pipeline


def basename(path):
    basefilename = os.path.split(path)[1]
    return os.path.splitext(basefilename)[0]


@click.command(help='Process an image to extract its fields')
@click.argument('modelpath')
@click.argument('image')
@click.argument('side')
@click.option('--fromtruth', type=click.File(),
              help='Use a ground-truth json file and skip segmentation '
              'and polygon-detection')
@click.option('--debug', default=False, type=bool,
              help='Save all intermediary images in the pipeline to files '
              'in the current folder')
def cli(modelpath, image, side, fromtruth, debug):
    if fromtruth is not None:
        poly = np.array(json.load(fromtruth)['quad'])
    else:
        poly = None
    if debug:
        debug = basename(image)
    fields = pipeline.process(modelpath, cv2.imread(image), side,
                              poly=poly, debug=debug)
    # TODO: uncomment once `pipeline` return fields
    # printed_fields = '\n'.join(['{} => {}'.format(name, value)
    #                             for name, value in fields])
    # click.echo(printed_fields)


if __name__ == '__main__':
    cli()
