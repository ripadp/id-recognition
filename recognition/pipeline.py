# import json
import logging
from functools import lru_cache

import cv2
import numpy as np
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam
from .model import IoU

from . import constants


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def order_points(pts):
    """Order the coordinates in `pts` as top-left, top-right, bottom-right,
    bottom-left."""

    # initialze a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype=np.float32)

    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    assert pts.shape[0] == 4
    sum = pts.sum(1)
    rect[0] = pts[np.argmin(sum)]
    rect[2] = pts[np.argmax(sum)]

    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    # return the ordered coordinates
    return rect


def segmented_contours(seg, is_gray=True):
    """Extract contours from a segmented image."""

    if not is_gray:
        seg = cv2.cvtColor(seg, cv2.COLOR_BGR2GRAY)
    _, binary = cv2.threshold(seg, 50, 255, cv2.THRESH_BINARY)
    contours, _ = cv2.findContours(binary, cv2.RETR_LIST,
                                   cv2.CHAIN_APPROX_TC89_L1)
    return contours


def contour_as_poly(contour, eps_factor=0.01):
    """Approximate a contour to a polygon."""

    hull = cv2.convexHull(contour, clockwise=True)
    eps = eps_factor * cv2.arcLength(hull, True)
    poly = cv2.approxPolyDP(hull, eps, True)
    assert poly.shape[1:] == (1, 2)
    return np.squeeze(poly)


@lru_cache()
def init_segmentation(modelpath):
    model = load_model(modelpath, compile=False)
    model.compile(optimizer=Adam(1e-4), loss=IoU, metrics=['binary_accuracy'])
    return model


def segment(model, image):
    """Run the segmentation model on `image` and return the
    binary result image."""
    in_size = constants.MODEL_INPUT_SIZE

    height, width = image.shape[:2]
    image = cv2.resize(image, (in_size, in_size))
    image = image / 255.0

    predict = model.predict(image.reshape(1, in_size, in_size, 3))[0]
    predict = cv2.resize(predict, (width, height))
    return (255 * predict).astype(np.uint8)


def detect_poly(image):
    """Detect a 4-vertices polygon from a segmentation image."""
    contours = segmented_contours(image)
    n_contours = len(contours)
    assert n_contours != 0, 'No contours found'
    logger.info('Found %s contour(s)', n_contours)

    # Take the biggest contour
    areas = list(map(cv2.contourArea, contours))
    max_areas_id = np.argmax(areas)
    logger.info('Taking contour %s, has maximum area', max_areas_id)
    contour = contours[max_areas_id]

    # Reduce precision until we find a 4-vertices polygon
    for eps_factor in np.linspace(0.01, 0.03, num=21):
        poly = contour_as_poly(contour, eps_factor=eps_factor)
        n_vertices = poly.shape[0]
        assert n_vertices >= 4, 'No 4-vertices polygon found'
        if n_vertices == 4:
            break
    assert n_vertices == 4, 'No 4-vertices polygon found'

    logger.info('Approxmiated to 4-vertices polygon')
    return poly


def draw_poly(image, poly):
    """Draw `poly` on `image`."""
    point_radius = int(image.shape[0] / 100)
    point_color = (0, 255, 255)
    line_color = (0, 100, 255)

    poly_image = image.copy()
    for point in poly:
        cv2.circle(poly_image, tuple(point), point_radius, point_color, 2)
    cv2.polylines(poly_image, [np.array(poly).reshape(-1, 1, 2)],
                  True, line_color, 2)

    return poly_image


def correct(image, poly):
    """Extract from `image` the rectangle defined by `poly`, with corrected
    perspective."""

    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(poly)
    tl, tr, br, bl = rect

    # compute the maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    width_bottom = np.linalg.norm(br - bl)
    width_top = np.linalg.norm(tr - tl)
    width = max(width_bottom, width_top)
    height = width / constants.ASPECT_RATIO

    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([[0, 0],
                    [width - 1, 0],
                    [width - 1, height - 1],
                    [0, height - 1]],
                   dtype=np.float32)

    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    return cv2.warpPerspective(image, M, (int(width), int(height)))


def filter_(image):
    raise NotImplementedError


def cut_fields(image, side):
    raise NotImplementedError


def ocr_fields(field_images):
    raise NotImplementedError


def process(modelpath, image, side, poly=None, debug=False):
    assert side in ['front', 'back']

    # Segmentation
    if poly is None:
        model = init_segmentation(modelpath)
        segment_image = segment(model, image)
        if debug is not False:
            cv2.imwrite(debug + '-1-segment.jpg', segment_image)

    # Polygon extraction
    if poly is None:
        poly = detect_poly(segment_image)
    poly_image = draw_poly(image, poly)
    if debug is not False:
        cv2.imwrite(debug + '-2-poly.jpg', poly_image)

    # Perspective correction
    corrected_image = correct(image, poly)
    if debug is not False:
        cv2.imwrite(debug + '-3-corrected.jpg', corrected_image)

    # # Filtering
    # filtered_image = filter_(corrected_image)
    # if debug is not False:
    #     cv2.imwrite(debug + '-4-filtered.jpg', filtered_image)

    # # Field cutting
    # field_images = cut_fields(filtered_image, side)
    # if debug is not False:
    #     for field_name, field_image in field_images.items():
    #         cv2.imwrite(debug + '-5-field={}.jpg'.format(field_name),
    #                     field_image)

    # # Field OCR
    # fields = ocr_fields(field_images)
    # if debug is not False:
    #     with open(debug + '-6-fields.json'.format(field_name), 'w') as f:
    #         json.dump(fields, f)

    # return fields
