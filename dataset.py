import os
import json
from glob import glob
from itertools import product
from random import sample

import click
import cv2
import numpy as np
from tqdm import tqdm


MAX_VIDEO_FRAMES = 500
EVERY_NTH_FRAME = 2
EDIT_MARGIN = 0.1
MAX_ASPECT_RATIO = 16 / 9
MIN_ASPECT_RATIO = 1 / MAX_ASPECT_RATIO


def mkdirp(path):
    """Recursively create the directories in `path`, with no effect
    if they exist."""

    if os.path.exists(path):
        return

    base, _ = os.path.split(path)
    if not os.path.exists(base):
        mkdirp(base)
    os.mkdir(path)


@click.group()
def cli():
    pass


def process_paths(path, subfolder_in, subfolder_out, postfix):
    """Process `path` (containing `subfolder_in`) to create the output path
    (containing `subfolder_out` and `postfix`)."""

    error = ('The path to the file should be of the form '
             '".../{}/<author>/<filename>"').format(subfolder_in)

    # Split `path` to get its parent parts
    basefolder, filename = os.path.split(path)
    basefolder, folder_author = os.path.split(basefolder)
    basefolder, basesubfolder = os.path.split(basefolder)

    # We expect the parts to be well-behaved
    assert basesubfolder == subfolder_in, error

    # Create the output folder and return the output file path
    outfolder = os.path.join(basefolder, subfolder_out, folder_author)
    outpath = os.path.join(outfolder, os.path.splitext(filename)[0] + postfix)
    mkdirp(outfolder)

    return outfolder, outpath


@cli.command(help='''Split a video into its frames.

For manual annotation and addition to the dataset. The path to the video
to split should be of the form ".../videos/<folder>/<name>.ext",
and the images will be saved to
".../images/<folder>/<name>-<frame-number>.png"''')
@click.argument('video')
@click.option('--every-nth-frame', default=EVERY_NTH_FRAME, type=int,
              help='Read every nth frame (default {})'.format(EVERY_NTH_FRAME))
def splitvideo(video, every_nth_frame):
    outfolder, imagepath = process_paths(video, 'videos', 'images', '-{}.png')

    click.echo('Processing video file from "{}"'.format(video))
    cv_video = cv2.VideoCapture(video)
    n = 0

    # Extract and save video frames
    while cv_video.isOpened():
        ret, frame = cv_video.read()
        for _ in range(every_nth_frame - 1):
            cv_video.grab()
        if ret is False:
            break
        cv2.imwrite(imagepath.format(n * every_nth_frame), frame)
        n += 1
        if n >= MAX_VIDEO_FRAMES:
            click.echo('Stopping at {} frames (though the video may be longer)'
                       .format(MAX_VIDEO_FRAMES))
            break

    click.echo('Saved {} frames to "{}"'.format(n, outfolder))


class Editor:

    """Editor to enter point annotations on an image."""

    point_color = (0, 255, 255)
    line_color = (0, 100, 255)
    help = '''
Instructions
------------

Click to add a new point.
Drag (grabbing inside the yellow circle) to drag a point around.
Press:
  - 'q' to quit
  - 'r' to remove the last point entered
  - 'c' to clear all points
  - '-' to zoom out and restart
  - '+' to zoom in and restart
  - 'enter' to save the results (only works if you have 4 points)
'''

    def __init__(self, imagepath, margin_factor=EDIT_MARGIN):
        # Copy the original image to make one we'll draw on, with margins
        self.imagepath = imagepath
        self.orig_image = cv2.imread(imagepath)
        self.height, self.width, channels = self.orig_image.shape
        self.margin_factor = margin_factor
        self.margin_height = int(self.height * margin_factor)
        self.margin_width = int(self.width * margin_factor)
        self.edit_shape = (self.height + 2 * self.margin_height,
                           self.width + 2 * self.margin_width,
                           channels)
        self.edit_image = np.zeros(self.edit_shape, self.orig_image.dtype)
        self.edit_image[self.margin_height:self.height + self.margin_height,
                        self.margin_width:self.width + self.margin_width,
                        :] = self.orig_image.copy()

        # Parameters for drawing points and lines
        self.point_radius = int(self.orig_image.shape[0] / 10)

        # The actual points annotated and which one we're dragging (if any)
        self.points = []
        self.drag_idx = None
        self.drag_offset = None

        # Create a window and bind our click handler to it
        self.window_name = os.path.join(imagepath)
        cv2.namedWindow(self.window_name)
        cv2.setMouseCallback(self.window_name, self.click_handler)

    def closest_points(self, x, y):
        """Get elements in `self.points` that have distance
        to `(x, y)` <= `self.point_radius`."""

        if len(self.points) > 0:
            apoints = np.array(self.points)
            distances = np.linalg.norm(apoints - [x, y], axis=1)
            return np.where(distances < self.point_radius)[0]
        else:
            return np.array([])

    def refresh(self):
        """Redraw the editor image, with current points."""
        self.edit_image[:] = 0
        self.edit_image[self.margin_height:self.height + self.margin_height,
                        self.margin_width:self.width + self.margin_width,
                        :] = self.orig_image.copy()
        for point in self.points:
            cv2.circle(self.edit_image, point,
                       self.point_radius, self.point_color, 2)
        cv2.polylines(self.edit_image,
                      [np.array(self.points).reshape(-1, 1, 2)],
                      True, self.line_color, 2)
        cv2.imshow(self.window_name, self.edit_image)

    def restart(self, imagepath, margin_factor, truthpath):
        cv2.destroyAllWindows()
        self.__init__(imagepath, margin_factor)
        return self.run(truthpath)

    def click_handler(self, event, x, y, flags, param):
        refresh = False

        if event == cv2.EVENT_LBUTTONDOWN:
            closest_points = self.closest_points(x, y)
            if len(closest_points) > 0:
                # Drag the closest point (one of the closest,
                # if several are very close)
                self.drag_idx = closest_points[0]
                tx, ty = self.points[self.drag_idx]
                self.drag_offset = (tx - x, ty - y)
            else:
                # Add a point and drag it
                self.points.append((x, y))
                self.drag_idx = len(self.points) - 1
                self.drag_offset = (0, 0)
            refresh = True

        if event == cv2.EVENT_LBUTTONUP:
            self.drag_idx = None
            self.drag_offset = None

        if (event == cv2.EVENT_MOUSEMOVE
                and self.drag_idx is not None
                and len(self.points) >= 1):
            ox, oy = self.drag_offset
            self.points[self.drag_idx] = (ox + x, oy + y)
            refresh = True

        if refresh:
            self.refresh()

    def points_in_orig(self):
        return [(x - self.margin_width, y - self.margin_height)
                for x, y in self.points]

    def run(self, truthpath):
        """Run the event loop."""

        click.echo(self.help)
        saved = False

        while True:
            cv2.imshow(self.window_name, self.edit_image)
            key = cv2.waitKey()

            if key == ord('q'):
                click.echo('Quitting')
                break

            if key == ord('c'):
                click.echo('Restarting with no points')
                self.points.clear()
                self.refresh()

            if key == ord('r') and len(self.points) >= 1:
                click.echo('Removing last point')
                self.points.pop()
                self.refresh()

            if key == ord('-'):
                click.echo('Zooming out and restarting')
                return self.restart(self.imagepath, self.margin_factor * 1.2,
                                    truthpath)

            if key == ord('+'):
                click.echo('Zooming in and restarting')
                return self.restart(self.imagepath, self.margin_factor / 1.2,
                                    truthpath)

            if key == 13:
                n_points = len(self.points)
                if n_points == 4:
                    click.echo('Done: {}'.format(self.points))
                    click.echo('Saving results to "{}"'.format(truthpath))
                    with open(truthpath, 'w') as f:
                        json.dump({'quad': self.points_in_orig()}, f)
                    saved = True
                    break
                else:
                    click.echo('{} points entered, must be 4 to save'
                               .format(n_points))

        cv2.destroyAllWindows()
        if saved:
            return self.points_in_orig()


@cli.command(help='''GUI to identify a ground truth segmentation

The path to the image should be of the form
".../images/<folder>/<name>.ext",
and the ground truth file will be saved to
".../ground-truch/<folder>/<name>.json"''')
@click.argument('image')
def annotate(image):
    _, truthpath = process_paths(image, 'images', 'ground-truth', '.json')
    editor = Editor(image)
    confirm_msg = 'No points saved. Do you want to try again?'
    while (editor.run(truthpath) is None
           and click.confirm(confirm_msg, prompt_suffix='')):
        pass


@cli.command(help='pack the dataset for storage on a private '
             'location online')
def pack():
    raise NotImplementedError


@cli.command(help='download and install an annotated dataset '
             '(built with `pack`) from an url')
def get():
    raise NotImplementedError


def read_image(image_path, truth_path):
    image = cv2.imread(image_path)
    mask = np.zeros(image.shape[:2], dtype=np.uint8)

    with open(truth_path, 'r') as f:
        coords = np.array(json.load(f)['quad'])
    cv2.fillPoly(mask, coords.reshape(-1, 4, 2), color=255)
    assert set(mask.flatten()).issubset([0, 255])

    return image, mask, coords


def build_crops(image, mask, coords, n_crops):
    height, width = image.shape[:2]
    step = min(int(width / 5), int(height / 5))

    n_right = (width - coords[:, 0].max()) // step
    n_top = coords[:, 1].min() // step
    n_left = coords[:, 0].min() // step
    n_bottom = (height - coords[:, 1].max()) // step
    if n_right < 0:
        n_right = 0
    if n_top < 0:
        n_top = 0
    if n_left < 0:
        n_left = 0
    if n_bottom < 0:
        n_bottom = 0

    # Build the actual cropped images
    images = []
    masks = []
    for right, top, left, bottom in \
            product(range(n_right + 1), range(n_top + 1),
                    range(n_left + 1), range(n_bottom + 1)):
        current_width = width - step * (right + left)
        current_height = height - step * (top + bottom)
        aspect = current_width / current_height
        if aspect < MIN_ASPECT_RATIO or aspect > MAX_ASPECT_RATIO:
            continue
        images.append(image[top * step:height - bottom * step,
                            left * step:width - right * step])
        masks.append(mask[top * step:height - bottom * step,
                          left * step:width - right * step])

    # Return only up to n_crops crops
    crop_images = images[1:]
    crop_masks = masks[1:]
    if len(crop_images) > n_crops:
        crop_images = sample(crop_images, n_crops)
        crop_masks = sample(crop_masks, n_crops)

    return [image] + crop_images, [mask] + crop_masks


def read_process_image(image_path, truth_path, n_crops):
    image, mask, coords = read_image(image_path, truth_path)

    # Compute crops if asked to
    if n_crops > 0:
        images, masks = build_crops(image, mask, coords, n_crops)
    else:
        images = [image]
        masks = [mask]

    # Resize to what the model expects
    for i in range(len(images)):
        images[i] = cv2.resize(images[i], (256, 256))
        masks[i] = np.expand_dims(cv2.resize(masks[i], (256, 256)), axis=2)

    return images, masks


@cli.command(help='''Preprocess all images and masks into npz files.''')
@click.argument('datasetpath')
@click.option('--n-crops', default=0, type=int,
              help='Augment the dataset with crops of the images')
def preprocess(datasetpath, n_crops):
    Y_paths = glob(os.path.join(datasetpath, 'ground-truth/*/*.json'))
    X_paths = [(path
                .replace('/ground-truth/', '/images/')
                .replace('.json', '.png'))
               for path in Y_paths]

    # Load all images
    click.echo('Stacking all images and labels into numpy arrays')
    if n_crops > 0:
        click.echo('... and adding up to {} cropped augmentations '
                   'per image of the dataset'.format(n_crops))
    images = []
    labels = []
    for image_path, truth_path in zip(tqdm(X_paths), Y_paths):
        current_images, current_labels = \
            read_process_image(image_path, truth_path, n_crops)
        images.extend(current_images)
        labels.extend(current_labels)
    images = np.array(images)
    labels = np.array(labels)
    assert images.dtype == np.uint8
    assert labels.dtype == np.uint8

    # Save the dataset to an npz file
    click.echo('Saving numpy arrays to {}'
               .format(os.path.join(datasetpath, 'dataset.npz')))
    np.savez_compressed(os.path.join(datasetpath, 'dataset.npz'),
                        images=images,
                        labels=labels)


if __name__ == '__main__':
    cli()
