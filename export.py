import click
import tensorflow as tf
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam

from recognition.model import IoU


@click.command(help='Export a Keras model to TF Lite')
@click.argument('modelpath')
@click.argument('tflitepath')
def cli(modelpath, tflitepath):
    click.echo('Loading Keras model from {}'.format(modelpath))
    model = load_model(modelpath, compile=False)
    model.compile(optimizer=Adam(1e-4), loss=IoU, metrics=['binary_accuracy'])

    click.echo('Exporting Keras model to TF Lite at {}'.format(tflitepath))
    tflite_model = tf.lite.TFLiteConverter.from_keras_model(model).convert()
    with open(tflitepath, 'wb') as f:
        f.write(tflite_model)


if __name__ == '__main__':
    cli()
