from itertools import product

import click
import numpy as np
from sklearn.model_selection import train_test_split
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint
from tqdm import tqdm

from recognition.model import unet


BATCH_SIZE = 50


def training_gen(X, Y, batch_size):
    while True:
        for start in range(0, X.shape[0], batch_size):
            yield (X[start:start + batch_size] / 255,
                   Y[start:start + batch_size] / 255)


def rotate_90deg(image):
    axes = np.arange(len(image.shape))
    axes[[0, 1]] = axes[[1, 0]]
    return np.transpose(image, axes)[::-1]


@click.group()
def cli():
    pass


@cli.command(help='''Train the segmentation model''')
@click.argument('datasetpath')
@click.argument('checkpointpath')
@click.option('--augment-rotations', default=True, type=bool,
              help='Augment the dataset with rotations of the images')
def segmentation(datasetpath, checkpointpath, augment_rotations):
    click.echo('Loading preprocessed dataset npz files')
    dataset = np.load(datasetpath)
    X = dataset['images']
    Y = dataset['labels']
    n_images = X.shape[0]

    # Prepare the train/validation arrays with rotations
    if augment_rotations:
        click.echo('Adding rotations to dataset')
        n_rotations = 4
        X = np.repeat(X, n_rotations, axis=0)
        Y = np.repeat(Y, n_rotations, axis=0)
        for i, rot in product(tqdm(range(n_images)), range(1, n_rotations)):
            idx = i * n_rotations + rot
            X[idx] = rotate_90deg(X[idx - 1])
            Y[idx] = rotate_90deg(Y[idx - 1])
    X_train, X_val, Y_train, Y_val = \
        train_test_split(X, Y, shuffle=True, test_size=0.1)

    # Get the model, set up viz and checkpointing, then train
    click.echo('Training')
    model = unet()
    board = TensorBoard()
    checkpointer = ModelCheckpoint(checkpointpath,
                                   monitor='val_loss', verbose=1,
                                   save_best_only=True)
    model.fit_generator(
        generator=training_gen(X_train, Y_train, BATCH_SIZE),
        steps_per_epoch=np.ceil(X_train.shape[0] / BATCH_SIZE),
        epochs=100, callbacks=[board, checkpointer],
        validation_data=training_gen(X_val, Y_val, BATCH_SIZE),
        validation_steps=np.ceil(X_val.shape[0] / BATCH_SIZE)
    )


if __name__ == '__main__':
    cli()
